import { SaveFile } from './savefile';

async function main() {
  const [path] = process.argv.slice(2);

  if (!path) {
    console.error('Usage: npm run start <path-to-a-save-file>');
    process.exit(1);
  }

  const saveFile = new SaveFile(path);

  console.time('Unpacked in');
  await saveFile.unpack().catch(handleError);
  console.timeEnd('Unpacked in');

  console.time('Packed in');
  await saveFile.pack().catch(handleError);
  console.timeEnd('Packed in');

  saveFile.clearIntermediateFiles();
}

function handleError(error: Error) {
  console.error(error.message);
  process.exit(1);
}

main();
