import { createWriteStream, existsSync, unlink, NoParamCallback } from 'fs';
import { open } from 'yauzl';
import { ZipFile } from 'yazl';

export class SaveFile {
  private path: string;
  readonly entries: string[] = [];

  constructor(path: string) {
    this.path = path;

    if (!existsSync(this.path)) {
      console.error('File not found:', this.path);
      process.exit(1);
    }
  }

  unpack(): Promise<void> {
    return new Promise((resolve, reject) => {
      open(this.path, (error, archive) => {
        if (error || !archive) {
          return reject(error || new Error('No etries in archive'));
        }

        const writePromises: Promise<void>[] = [];

        archive.on('entry', (entry) => {
          this.entries.push(entry.fileName);

          archive.openReadStream(entry, (error, stream) => {
            if (error || !stream) {
              return reject(error || new Error("Can't read entry stream"));
            }

            const output = createWriteStream(entry.fileName);
            output.on('error', (outputError) => reject(outputError));

            writePromises.push(
              new Promise((writeResolve) => {
                output.on('finish', () => writeResolve());
              }),
            );

            stream.pipe(output);
          });
        });

        archive.on('end', () => {
          Promise.all(writePromises).then(() => resolve());
        });
      });
    });
  }

  pack(): Promise<void> {
    return new Promise((resolve, reject) => {
      const archive = new ZipFile();

      this.entries.forEach((name) => archive.addFile(name, name));

      const output = createWriteStream(this.path);
      output.on('error', (error) => reject(error));

      archive.outputStream.pipe(output).on('close', () => resolve());
      archive.end();
    });
  }

  clearIntermediateFiles() {
    const handleError: NoParamCallback = (error) => {
      if (error) {
        console.warn(error);
      }
    };

    this.entries.forEach((name) => unlink(name, handleError));
  }
}
