# STEdit (Stellaris save editor)

A CLI tool that speed-ups editing resources, experience, traits, and so on. Back-up save file before editing just to be safe.

## Installation

```bash
$ npm i
```

## Running the tool

```bash
$ npm run start <path-to-save-file>
```
